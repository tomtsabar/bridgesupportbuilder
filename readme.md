# Bridge Support Builder
![alt text](https://bitbucket.org/tomtsabar/bridgesupportbuilder/raw/master/presentation/bridge.PNG)
Bridge Support Builder is a Python package for 3D modeling of scaffolds supports.
The work is based on the Bridging the Gap paper written by Jérémie Dumas, Jean Hergel and Sylvain Lefebvre.
The full paper is avliable at https://members.loria.fr/JDumas/files/papers/scaffoldings/bridging-the-gap.pdf

## Python version

The project is tested using Python 3.6.11

## Running in docker

```bash
docker run --name bridge -itd pymesh/pymesh
docker exec -ti bridge bash
git clone https://tomtsabar@bitbucket.org/tomtsabar/bridgesupportbuilder.git
pip3 install -r ./bridgesupportbuilder/req.txt
wget https://github.com/slic3r/Slic3r/releases/download/1.3.0/slic3r-1.3.0-linux-x64.tar.bz2
tar xjf slic3r-1.3.0-linux-x64.tar.bz2

python -m bridgesupportbuilder build -p bridgesupportbuilder/models/bridge/bridge.stl -f stl
```

Now you can exit and re-enter the docker by:
```bash
docker exec -ti bridge bash
...
exit
```

## Running in linux (for optimistic only ;) )

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the projects dependencies.

```bash
pip install -r req.txt
```
Download slic3r from https://manual.slic3r.org/getting-slic3r/getting-slic3r#downloading.
Put the slic3r directory in the PATH environment variables.

python -m bridgesupportbuilder build -p bridgesupportbuilder/models/bridge/bridge.stl -f stl

## Usage
From outside of the packages directory

### Basic
Creates a support for a stl model.

```bash
python -m bridgesupportbuilder build -p bridgesupportbuilder/models/bridge/bridge.stl -f stl
```

### Save Intermidiate Results
Generate xyz intermidiate results of the points to support with:

```bash
python -m bridgesupportbuilder build -p bridgesupportbuilder/models/bridge/bridge.stl -f stl --xyz
```

Generate pickled intermidiate results of the 3D model of the support with:

```bash
python -m bridgesupportbuilder build -p bridgesupportbuilder/models/bridge/bridge.stl -f stl --pickle
```
### Use Intermidiate Results
Use xyz intermidiate results with:

```bash
python -m bridgesupportbuilder build -p bridgesupportbuilder/models/bridge/bridge.xyz -f xyz
```

Generate pickled intermidiate results of the 3D model of the support with:

```bash
python -m bridgesupportbuilder produce3d -p bridgesupportbuilder/models/bridge/bridge.pkl
```
## Contributors
Tom Tsabar
Omer Kafri

