from pygcode import Machine
from pygcode import Line
from pygcode import Word

from euclid3 import Point2
from euclid3 import Point3

import numpy as np

from skimage.draw import (line, polygon, disk,
                          circle_perimeter,
                          ellipse, ellipse_perimeter,
                          bezier_curve)

from PIL import Image as im
from scipy import ndimage
from scipy.spatial import ConvexHull, convex_hull_plot_2d
import matplotlib.pyplot as plt

from .utils import *
import time

DEBUG = True


def fromGcode(path, extremestPoint, xyz):
  """
  Get Gcode file path, generate 2D images of each slice and finds its support points.
  """

  start_time = time.time()

  printer3D = Machine()
  currentLayer = np.ones((nBedWidth, nBedHeight))
  lastLayer = np.ones((nBedWidth, nBedHeight))
 
  maxE = 0
  currentZ = 0

  supportPoints = []
  supportBridges = []
  supportStabilityPoints = []
 
  CoMPoints = []

  images = []
  lastVector = Point2(0,0)

  componentsDetails = None

  extremestGcodePoint = Point3(10000, 10000, 10000)

  with open(path, 'r') as fh:
    for lineText in fh.readlines():

      try:
        line = Line(lineText)
        
      except:
        if (DEBUG):
          print (lineText)
        continue

      printer3D.process_gcodes(*line.block.gcodes)

      if (checkIfNextLayer(printer3D, currentZ)):
        currentLayer, componentsDetails, firstLayer = unionComponents(lastLayer, currentLayer, componentsDetails)
        if (len(componentsDetails) > 0 and not firstLayer):
          stabilityBridges, componentsDetails = stabilityAnalysis(componentsDetails, currentLayer, currentZ)
          newPoints, newBridges = getNewSupports(stabilityBridges)
          supportStabilityPoints.extend(newPoints)
          supportBridges.extend(newBridges)

        currentZ, lastLayer, currentLayer = createNewLayer(currentLayer, printer3D)

      positionsArray, maxE = splitInjectingCommands(line, maxE, lastVector, printer3D)
        

      for tLastVector, tCurrentVector in positionsArray:
        
        checkIfNeedSupport(lastLayer, tCurrentVector, supportPoints, currentZ)
        updateLayer(currentLayer, tCurrentVector, tLastVector)
        

        #Find Extremest point for offsets calculations
        if (currentZ <= nlayerHeight*1.5):
          extremePointCandidate = Point3(round(tLastVector.x,2), round(tLastVector.y,2), 0)
          extremestGcodePoint = returnExtremest(extremestGcodePoint, extremePointCandidate)

          extremePointCandidate = Point3(round(tCurrentVector.x,2), round(tCurrentVector.y,2), 0)
          extremestGcodePoint = returnExtremest(extremestGcodePoint, extremePointCandidate)
            
      lastVector = printer3D.pos.vector

  # calc offsets
  xOffset = extremestGcodePoint.x - extremestPoint.x
  yOffset = extremestGcodePoint.y - extremestPoint.y
  zOffset = extremestGcodePoint.z - extremestPoint.z

  offsetVector = Vector3(xOffset, yOffset, zOffset)
  offsetVector2D = Vector2(xOffset, yOffset)

  movedSupportPoints = []
  for supportPoint in supportPoints:
    movedSupportPoints.append(supportPoint - offsetVector)

  movedStabilityPoints = []
  for supportPoint in supportStabilityPoints:
    movedStabilityPoints.append(supportPoint - offsetVector)

  movedSupportBridges = []

  for supportBridge in supportBridges:
    supportBridge.p1 -= offsetVector2D
    supportBridge.p2 -= offsetVector2D
    supportBridge.z -= offsetVector.z
    movedSupportBridges.append(supportBridge)

  if (xyz):
    with open(f'{path.replace(".gcode", ".xyz")}', 'w+') as fh:
      for sp in movedSupportPoints:
        fh.write (f'{sp.x} {sp.y} {sp.z} \n')

    with open(f'{path.replace(".gcode", ".stability.xyz")}', 'w+') as fh:
      for br in movedSupportBridges:
        fh.write (f'{br.p1.x} {br.p1.y} {br.z} \n')
        fh.write (f'{br.p2.x} {br.p2.y} {br.z} \n')
      for sp in movedStabilityPoints:
        fh.write (f'{sp.x} {sp.y} {sp.z} \n')

  movedSupportPoints.extend(movedSupportPoints)

  movedSupportBridges = [bridge.toBridge() for bridge in movedSupportBridges]

  print (f'Finished finding supports at {time.time() - start_time}')
  return movedSupportPoints, movedSupportBridges

def getNewSupports(stabilityBridges):
  supportPoints = []
  supportBridges = []
  for bridge in stabilityBridges:
    if (bridge.p1 == bridge.p2):
      supportPoints.append(Point3(bridge.p1.x / nModifier, bridge.p1.y / nModifier, bridge.z))
    else:
      bridge.p1.x /= nModifier
      bridge.p1.y /= nModifier
      bridge.p2.x /= nModifier
      bridge.p2.y /= nModifier
      supportBridges.append(bridge)

  return supportPoints, supportBridges

def unionComponents(lastLayer, currentLayer, componentsDetails):
  threshold = 50

  if (None == componentsDetails):
    labeled, nr_objects = ndimage.label(currentLayer > threshold)
    return labeled, [], True

  labeled, nr_objects = ndimage.label(currentLayer > threshold)

  if (0 == len(componentsDetails)):
    componentsDetails = calcLayerCoM(labeled, nr_objects)
    componentsDetails = initBoS(componentsDetails)

    return labeled, componentsDetails, True

  else:
    #Fix and union splitting components
    compMapping = mapPreviousToCurrentLayer(lastLayer, labeled, len(componentsDetails), unionDup = True)

    labeled, nr_objects = unionSplittingComponents(labeled, compMapping, nr_objects)

    #Handle merging and regular components
    compMapping = mapPreviousToCurrentLayer(lastLayer, labeled, len(componentsDetails), unionDup = False)
    layerComponentsDetails = calcLayerCoM(labeled, nr_objects)
    layerComponentsDetails = handleMergingAndRegularComponents(compMapping, layerComponentsDetails, componentsDetails )
    layerComponentsDetails = initBoS(layerComponentsDetails)

    return labeled, layerComponentsDetails, False
  return labeled, componentsDetails, False

def stabilityAnalysis(componentsDetails, currentLayer, currentZ):
  bridgeOuterPoints = []

  for comp in componentsDetails:
    discreteCoMCircle = generateDiscereteCircle(comp.getCoM(), nCoMSafetyRadius)
    discreteCoMEnlargeCircle = generateDiscereteCircle(comp.getCoM(), nCoMSafetyRadius * SafetyFactor)

    comp.updateBoS(discreteCoMCircle)


    hullPoints = [comp.BoS.points[i] for i in comp.BoS.vertices]
    compBridgeOuterPoints = getCommonPointsIndicesFromAnotherList(discreteCoMCircle, [Point2(p[0], p[1]) for p in hullPoints], discreteCoMEnlargeCircle)
    comp.updateBoS(compBridgeOuterPoints)

    bridgeOuterPoints.append(compBridgeOuterPoints)

  bridges = createBridgesToCloseComponentPoint(bridgeOuterPoints, currentLayer, currentZ)

  return bridges, componentsDetails

def createBridgesToCloseComponentPoint(bridgeOuterPoints, currentLayer, currentZ):

  bridgesByComponents = []
  for comp in bridgeOuterPoints:
    compBridges = []
    for point in comp:
      newBridge = WIPBridge(point, Point2(-9999,-9999), currentZ)
      newBridge.p2.is_open = False
      compBridges.append(newBridge)

    bridgesByComponents.append(compBridges)


  xs, ys = np.where(currentLayer != 0)
  for x,y in zip(xs, ys):
    color = currentLayer[x,y] - 1
    for bridge in bridgesByComponents[int(color)]:
      currentPoint = Point2(x,y)
      if Point2(-9999,-9999) == bridge.p2:
        bridge.p2 = currentPoint
      else:
        if (bridge.p1.distance(currentPoint) < bridge.p1.distance(bridge.p2)):
          bridge.p2 = currentPoint

  bridges = []
  for comp in bridgesByComponents:
     bridges.extend(comp)

  return bridges

def initBoS(componentsDetails):
  for comp in componentsDetails:
    if (True == comp.root):
      comp.updateBoS(comp.queue)
      comp.emptyQueue()
  return componentsDetails

      
def unionSplittingComponents(labeled, compMapping, colors):
  incLabel = 1
  labelDict = dict()

  for index in range(len(compMapping)):
    newLabelFlag = False

    for topColor in compMapping[index]:
      if (topColor not in labelDict):
        labelDict[topColor] = incLabel
        newLabelFlag = True

    if len(compMapping[index]) > 0 and newLabelFlag:
      incLabel += 1

  for topColor in range(1,colors+1):
    if topColor not in labelDict:
      labelDict[topColor] = incLabel
      incLabel += 1

  xs, ys = np.where(labeled != 0)
  for x,y in zip(xs, ys):
    labeled[x,y] = labelDict[labeled[x,y]]

  return labeled, incLabel - 1

def handleMergingAndRegularComponents(compMapping, layerComponentsDetails, componentsDetails):

  for index in range(len(compMapping)):
    compLen = len(compMapping[index])
    if (compLen == 0):
      continue
    if (compLen == 1):
      currentLayerComp = next(iter(compMapping[index]))
      layerComponentsDetails[currentLayerComp - 1].mergeInto(componentsDetails[index])
      layerComponentsDetails[currentLayerComp - 1].emptyQueue()
    else:
      print ("Error")
  return layerComponentsDetails


def mapPreviousToCurrentLayer(lastLayer, currentLayer, prevComponentsNum, unionDup = True):
  compMapping = []
  for index in range(prevComponentsNum):
    compMapping.append(set())

  xs, ys = np.where(lastLayer != 0)
  for x,y in zip(xs, ys):
    if lastLayer[x,y] != 0 and currentLayer[x,y] != 0:
      compMapping[lastLayer[x,y] - 1].add(currentLayer[x,y])

  if (unionDup):
    compMapping = unionDuplicates(compMapping)

  return compMapping

def unionDuplicates(compMapping):
  dupFound = unionFirstDuplicate(compMapping)
  while (dupFound):
    dupIndex = findDuplicateIndex(compMapping, dupFound)

    compMapping = unionAtIndex(dupIndex, dupFound, compMapping)
    dupFound = unionFirstDuplicate(compMapping)
  return compMapping

def unionFirstDuplicate(compMapping):
    tmpSet = set()
    for mapping in compMapping:
      for element in mapping:
        if (element in tmpSet):
          return element
        tmpSet.add(element)
    return None

def findDuplicateIndex(compMapping, dupFound):
  for i in range(len(compMapping)):
    if dupFound in compMapping[i]:
      return i

  print ("Algorithmic error, findDuplicateIndex")
  return None

def unionAtIndex(dupIndex, dupFound, compMapping):
  newCompMapping = [set()]
  for mapping in compMapping:
    if dupFound in mapping:
      for element in mapping:
        newCompMapping[0].add(element)
    else:
      newCompMapping.append(mapping)
  return newCompMapping

def calcLayerCoM(labeled, nr_objects):
  CompArray = []
  for label in range(nr_objects):
    CompArray.append(CumulativeComponent())

  xs, ys = np.where(labeled != 0)
  for x,y in zip(xs, ys):
    CompArray[labeled[x,y] - 1].addPixel(x,y)

  for label in range(nr_objects):
    CompArray[label].updateCoM(CompArray[label].queue)

  return CompArray

def checkIfNextLayer(printer3D, currentZ):
  return printer3D.pos.vector.z > currentZ and printer3D.pos.vector.z <= currentZ + nlayerHeight*2

def createNewLayer(currentLayer, printer3D):
        
  if (DEBUG):
    if (printer3D.pos.vector.z/2 == float(int(printer3D.pos.vector.z/2))):
      print (f'Processing layer: {printer3D.pos.vector.z / (layerHeight / 100)}')

  lastLayer = currentLayer
  currentLayer = np.zeros((nBedWidth, nBedHeight))

  return printer3D.pos.vector.z, lastLayer, currentLayer

def splitInjectingCommands(line, maxE, lastVector, printer3D):
  

  if len(line.block.modal_params) != 1 :
    return [], maxE


  cmd = line.block.modal_params[0]

  #Checks extruder works
  cmd = str(cmd)
  if ('E' in cmd):
    currentE = float(cmd[1:])

    #Wrap around when the extruder returns to 0 from max value
    if (currentE <= 0.01):
      maxE = 0

    #Checks extruder outputs material
    if (currentE > maxE):
      maxE = currentE

    if (Word('G', 1) == line.block.gcodes[0].word_key):
      lastPoint = Point2(lastVector.x, lastVector.y)
      currentPoint = Point2(printer3D.pos.vector.x, printer3D.pos.vector.y)

      direction = currentPoint - lastPoint
      nDirection = direction.normalize() * nMaxBridgeLength
      positionsArray = []
      newPoint = lastPoint + nDirection

      while (lastPoint.distance(currentPoint) > lastPoint.distance(newPoint)):
        positionsArray.append((newPoint - nDirection, newPoint))
        newPoint = newPoint + nDirection

      positionsArray.append((newPoint - nDirection, currentPoint ))
      return positionsArray, maxE
      
    else:
      return [(lastVector, printer3D.pos.vector)], maxE

  return [], maxE

def updateLayer(layer, currentVector, lastVector):
  nCX = (int) (currentVector.x * nModifier)
  nCY = (int) (currentVector.y * nModifier)
  nLX = (int) (lastVector.x * nModifier)
  nLY = (int) (lastVector.y * nModifier)

  rr, cc = disk((nCX, nCY), nPrintNozzle, shape=layer.shape)
  layer[rr, cc] = 100

  diffX = nLX - nCX
  diffY = nLY - nCY
  for x,y in zip(rr, cc):
    x = max(0, x)
    y = max(0, y)
    xplus = min(nBedWidth - 1, x + diffX)
    yplus = min(nBedHeight - 1, y + diffY)

    rrr, ccc = line(x, y, xplus, yplus)

    layer[rrr, ccc] = 100

  
def checkIfNeedSupport(lastLayer, currentPos, supportPoints, currentZ):
  
  if (currentZ <= nlayerHeight * 1.5):
    return

  nCX = (int) (currentPos.x * nModifier)
  nCY = (int) (currentPos.y * nModifier)

  rr, cc = disk((nCX, nCY), nPrintNozzle, shape=lastLayer.shape)
  supported = 0
  unsupported = 0

  for x,y in zip(rr, cc):
    
    if (0 == lastLayer[x, y]):
      unsupported += 1
    else:
      supported += 1

  if (unsupported > supported):
    
    newSupportPoint = Point3(currentPos.x, currentPos.y, currentZ)
   
    if (not cancelCloseSupports(newSupportPoint, supportPoints)):
      supportPoints.append(newSupportPoint)
    
def cancelCloseSupports(newSupportPoint, supportPoints):
  for index in range(len(supportPoints)-1, -1, -1):
      if (supportPoints[index].distance(newSupportPoint) < nCancelingDistance):
        return True

      if (supportPoints[index].z != newSupportPoint.z):
        return False
  return False
