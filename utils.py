from euclid3 import Point3, Vector3, Point2, Vector2, LineSegment2, LineSegment3
from scipy.spatial import ConvexHull

# The unit are 1/100 of mm (millimeters)
bedWidth = 200_00
bedHeight = 200_00
printNozzle = 40
pixelResulution = 20
maxBridgeLength = 5_00
comSafetyRadius = 5_00
cancelingDistance = 3_00
layerHeight = 20


nBedWidth = bedWidth // pixelResulution
nBedHeight = bedHeight // pixelResulution
nModifier = 100 // pixelResulution
nPrintNozzle = printNozzle // pixelResulution

nCoMSafetyRadius =  round((comSafetyRadius / 100) * nModifier)
nlayerHeight = layerHeight / 100
nMaxBridgeLength = maxBridgeLength / 100
nCancelingDistance = cancelingDistance / 100

SafetyFactor = 2
lMin = 1.6 # millimeters
longestBridge = 30 # millimeters
longestPointTip = 5 # millimeters
sweepDirectionsAmount = 4
epsilon = 0.01
optionalBridgeHeightResolution = None

class SupportedPoint2(Point2):
  def __init__(self, x=0, y=0, is_open=True):
    super().__init__(x, y)
    self.is_open = is_open

class SupportedPoint(Point3):
  def __init__(self, x=0, y=0, z=0, is_open=True):
    super().__init__(x, y, z)
    self.is_open = is_open

def generateDiscereteCircle(center, radius):
  points = []

  sqrtHalf = 0.707

  points.append(Point2(int(center.x + radius), int(center.y)))
  points.append(Point2(int(center.x - radius), int(center.y)))
  points.append(Point2(int(center.x) , int(center.y + radius)))
  points.append(Point2(int(center.x) , int(center.y - radius)))

  points.append(Point2(int(center.x + radius*sqrtHalf), int(center.y + radius*sqrtHalf)))
  points.append(Point2(int(center.x + radius*sqrtHalf), int(center.y - radius*sqrtHalf)))
  points.append(Point2(int(center.x - radius*sqrtHalf), int(center.y + radius*sqrtHalf)))
  points.append(Point2(int(center.x - radius*sqrtHalf), int(center.y - radius*sqrtHalf)))

  return points

def getCommonPointsIndicesFromAnotherList(listA, listB, listATag):
  commonPoints = []
  for pointA, pointATag in zip(listA, listATag):
    for pointB in listB:
      if (pointA.x == pointB.x and pointA.y == pointB.y):
        commonPoints.append(pointATag)
  return commonPoints

class WIPBridge():
  def __init__(self, p1, p2, z):
    self._lineSegment = LineSegment2(p1, p2)
    self.p1 = toSupportedPoint2(self._lineSegment.p1)
    self.p2 = toSupportedPoint2(self._lineSegment.p2)
    self.z = z

  def __repr__(self):
    return f'A: {self.p1}, B: {self.p2}'

  def toBridge(self):
  	p1, p2 = toSupportedPoint(self.p1), toSupportedPoint(self.p2)
  	p1.z, p2.z = self.z, self.z
  	return Bridge(p1, p2)

class Bridge:
  def __init__(self, *args):
    self._lineSegment = LineSegment3(*args)
    self.p1 = toSupportedPoint(self._lineSegment.p1)
    self.p2 = toSupportedPoint(self._lineSegment.p2)
    self.isBuilt = False

  @property
  def length(self):
    return self.p1.distance(self.p2)

class CumulativeComponent():
  def __init__(self):
    self.pixelsSum = 0
    self.queue = []
    self.CoMX = -1
    self.CoMY = -1
    self.root = True
    self.BoS = None

  def addPixel(self, x, y):
    self.queue.append(Point2(x,y))

  def updateCoM(self, pixels):
    cumulativeX = self.CoMX * self.pixelsSum
    cumulativeY = self.CoMY * self.pixelsSum
    for pixel in pixels:
      cumulativeX += pixel.x
      cumulativeY += pixel.y
      self.pixelsSum += 1

    self.CoMX = cumulativeX / self.pixelsSum 
    self.CoMY = cumulativeY / self.pixelsSum

  def updateBoS(self, pixels):

    if (len(pixels) == 0):
      return

    tuplePoint = [(p.x, p.y) for p in pixels]

    if (None == self.BoS):
      self.BoS = ConvexHull(tuplePoint, incremental=True)
    else:
      self.BoS.add_points(tuplePoint)

  def emptyQueue(self):
    self.queue = []

  def getCoM(self):
    return Point2(round(self.CoMX), round(self.CoMY))

  def getBoS(self):
    return self.BoS

  def mergeInto(self, cComp):

    self.root = False

    cumulativeX = self.CoMX * self.pixelsSum
    cumulativeY = self.CoMY * self.pixelsSum

    cumulativeX += cComp.CoMX * cComp.pixelsSum
    cumulativeY += cComp.CoMY * cComp.pixelsSum

    self.pixelsSum = self.pixelsSum + cComp.pixelsSum

    self.CoMX = cumulativeX / self.pixelsSum 
    self.CoMY = cumulativeY / self.pixelsSum

    if (None == self.BoS):
      self.BoS = cComp.BoS
    else:
      self.BoS.add_points([cComp.BoS.points[i] for i in cComp.BoS.vertices])

  
def toPoint2(point):
  return Point2(point.x, point.y)

def toPoint3(point):
  return Point3(point.x, point.y, point.z if hasattr(point, "z") else 0)

def toSupportedPoint(point):
  return SupportedPoint(point.x, point.y, point.z if hasattr(point, "z") else 0, 
  	point.is_open if hasattr(point, "is_open") else True)

def toSupportedPoint2(point):
  return SupportedPoint2(point.x, point.y, point.is_open if hasattr(point, "is_open") else True)

def toVector2(point):
  return Vector2(point.x, point.y)

def toVector3(point):
  return Vector3(point.x, point.y, point.z if hasattr(point, "z") else 0)

def returnExtremest(a, b):
  if (a.z < b.z):
    return a
  if a.z == b.z:
    if (a.y > b.y):
      return a
    if a.y == b.y:
      if (a.x >= b.x):
        return a
  return b