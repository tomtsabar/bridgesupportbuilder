from euclid3 import LineSegment2, Vector3, Point2
from .csgUtils import *
from .utils import *

class IntersectionPoint:
  def __init__(self, segment, p, maxZ, tipLength, order):
    self.segment = segment
    self.p = p
    self.maxZ = maxZ
    self.tipLength = tipLength
    self.order = order

class AnchoringSegment(LineSegment2):
  def __init__(self, z, p1, p2):
    super().__init__(toPoint2(p1), toPoint2(p2)) 
    self.z = z

  def intersectSweepLine(self, sweepLine, sweepDirection):
    pass

  def snap(self):
    pass

class SegmentForBridge(AnchoringSegment):
  def __init__(self, bridge, is_p1, startPoint, vector):
    super().__init__(bridge.p1.z if is_p1 else bridge.p2.z, startPoint, startPoint + vector)
    self.bridge = bridge
    self.is_p1 = is_p1
    self.z = self.bridge.p1.z

  def intersectSweepLine(self, sweepLine, sweepDirection):
    intersection = self.intersect(sweepLine)
    if intersection is None:
      return None

    maxZ = self.z - lMin
    tipLength = lMin
    order = sweepLine.v.dot(intersection)

    return IntersectionPoint(self, intersection, maxZ, tipLength, order)

  @property
  def point(self):
    if self.is_p1:
      return self.bridge.p1
    else:
      return self.bridge.p2
  

  def snap(self, intersection, z):
    res = []
    if self.is_p1:
      self.bridge.p1.is_open = False
      self.bridge.p1.x = intersection.p.x
      self.bridge.p1.y = intersection.p.y
      createStraightPillar(self.bridge.p1, intersection.p)
    else:
      self.bridge.p2.is_open = False
      self.bridge.p2.x = intersection.p.x
      self.bridge.p2.y = intersection.p.y
      createStraightPillar(self.bridge.p2, intersection.p)

    if not self.bridge.p1.is_open and not self.bridge.p2.is_open:
      res.append(createBridge2(self.bridge))
      self.bridge.isBuilt = True

    return res

class SegmentForPoint(AnchoringSegment):
  def __init__(self, point, p1, p2):
    super().__init__(point.z, p1, p2)
    self.point = point
    self.z = self.point.z

  def intersectSweepLine(self, sweepLine, sweepDirection):
    intersection = self.intersect(sweepLine)
    if intersection is None:
      return None

    d = intersection.distance(toPoint2(self.point))
    if d > lMin:
      maxZ = self.z - d
      tipLength = (2 ** 0.5) * d
    else:
      maxZ = self.z - lMin
      tipLength = (2 ** 0.5) * d + (lMin - d)

    order = sweepLine.v.dot(intersection)

    return IntersectionPoint(self, intersection, maxZ, tipLength, order)

  def snap(self, intersection, z):
    res = []
    self.point.is_open = False

    d = intersection.p.distance(toPoint2(self.point))

    tipEnd = Point3(intersection.p.x, intersection.p.y, self.point.z - d)
    intersectionOnBridge = Point3(intersection.p.x, intersection.p.y, z)
    res.append(createTip(self.point, tipEnd))
    res.append(createStraightPillar(tipEnd, intersectionOnBridge))

    return res