import os
import stl
from .utils import *

def fromSTL(stlPath):
  """
  """
  params = '--nozzle-diameter 0.4 --skirts 0 --fill-pattern rectilinear --fill-density 99% --first-layer-height 0.2 --layer-height 0.2 --perimeters 1 --top-solid-layers 0 --bottom-solid-layers 0'

  if (os.name == 'nt'):
      os.system(f'Slic3r-console {params} {stlPath}')
  else:
    current_dir = os.getcwd()
    os.chdir('/root/Slic3r')
    os.system(f'./Slic3r {params} ../{stlPath}')
    os.chdir(current_dir)

  extremestPoint = findLowestPoint(stl.mesh.Mesh.from_file(stlPath))

  return extremestPoint

def findLowestPoint(mesh):

  extremestPoint = Point3(100000, 100000, 100000)
  for point in mesh.points:

    p1 = Point3(round(point[0], 2), round(point[1], 2), round(point[2], 2))
    p2 = Point3(round(point[3], 2), round(point[4], 2), round(point[5], 2))
    p3 = Point3(round(point[6], 2), round(point[7], 2), round(point[8], 2))

    extremestPoint = returnExtremest(extremestPoint, p1)
    extremestPoint = returnExtremest(extremestPoint, p2)
    extremestPoint = returnExtremest(extremestPoint, p3)

  return extremestPoint



  


