import os
import sys
import click
import pymesh
from .gcodeBuilder import fromSTL
from .supportPoints import fromGcode
from .bridgeBuilder import fromXYZ
from .bridgeBuilder import fromXYZFile
from .bridgeBuilder import test_bridge_builder
from .csgUtils import produce3DFromPickle, saveSTL

@click.group()
def cli():
    pass


@click.command()
@click.option('-f', '--file-format', type=click.Choice(['stl', 'xyz']))
@click.option('-p', '--path' , help='model path')
@click.option('-k', '--pickle', is_flag=True, help="Save a .pkl file of the support parts.")
@click.option('-x', '--xyz', is_flag=True, help="Save .xyz files for intermediate results ")
def build(path, file_format, pickle, xyz):
    """
    
    """

    if (path is None or file_format is None):
    	print ('Usage: python -m BridgeSupportBuilder build -p <path_to_file> -f [stl|gcode|xyz]')
    	return -1

    path_no_ext = os.path.splitext(path)[0]

    picklePath = None
    if pickle:
        picklePath = path_no_ext + ".pkl"

    if (file_format == 'stl'):
        extremestPoint = fromSTL(path)
        points, bridges = fromGcode(path_no_ext + '.gcode', extremestPoint, xyz)
        csg = fromXYZ(points, bridges, picklePath)

    elif (file_format == 'xyz'):
        csg = fromXYZFile(path, picklePath)

    else:
        print ("Unsupported format")
        return -1

    saveSTL(csg, path_no_ext + "_support.stl")
    
    return 0
    
@click.command()
def test():
    test_bridge_builder()
    
@click.command()
@click.option('-p', '--path' , help='pickle path')
@click.option('-n', '--num-of-parts', help='Maximal amount of parts to take from the pickle file', type=int, default=None)
def produce3d(path, num_of_parts):
    csg = produce3DFromPickle(path, num_of_parts)

    path_no_ext = os.path.splitext(path)[0]
    saveSTL(csg, path_no_ext + "_pkl.stl")

if __name__ == '__main__':
    cli.add_command(build)
    cli.add_command(test)
    cli.add_command(produce3d)
    cli()