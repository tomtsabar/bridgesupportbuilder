from .utils import *
from euclid3 import Vector3, Plane, Point3, Point2
import math
from tqdm import tqdm
import pymesh
import numpy as np

layerHeightMM = layerHeight / 100
layerRadius = layerHeightMM * 3

tipCrossWidth = 2
tipCrossLength = 4

pillarCrossWidth = 2
pillarCrossLength = 4

raftRadius = 3

class DetailedCSG:
    def __init__(self, csg, details):
        self.csg = csg
        self.details = details

def translate(mesh, x, y, z):
    return pymesh.form_mesh(mesh.vertices + [[x, y, z]], mesh.faces)

def rotate(mesh, axis, angle):
    rot = pymesh.Quaternion.fromAxisAngle(axis, angle)
    rot = rot.to_matrix()
    return pymesh.form_mesh(np.dot(rot, (mesh.vertices).T).T, mesh.faces)

def toNPArray3(p):
    if type(p) is Point3:
        return np.array([p.x, p.y, p.z])
    if type(p) is Point2:
        return np.array([p.x, p.y, 0])

    return p

def createTip(p1, p2):
    try:
        p1 = toNPArray3(p1)
        p2 = toNPArray3(p2)

        center = (p1 + p2) / 2
        height = np.linalg.norm(p1-p2)
        if height < epsilon:
            return DetailedCSG(None, ("Empty Tip", p1, p2))

        radius = np.array([tipCrossWidth * layerHeightMM, tipCrossLength * layerHeightMM, height / 2])
        m1 = pymesh.generate_box_mesh(-radius, +radius)
        radius = np.array([tipCrossLength * layerHeightMM, tipCrossWidth * layerHeightMM, height / 2])
        m2 = pymesh.generate_box_mesh(-radius, +radius)
        result = pymesh.boolean(m1, m2, "union")
        result = rotate(result, [0, 0, 1], 45)

        directionVector = p2 - p1
        rot = pymesh.Quaternion.fromData([0, 0, 1], directionVector)
        rot = rot.to_matrix()
        result = pymesh.form_mesh(np.dot(rot, (result.vertices).T).T, result.faces)

        result = translate(result, center[0], center[1], center[2])
    
        return DetailedCSG(result, ("Tip", p1, p2))
    except:
        return DetailedCSG(None, ("Failed Tip", p1, p2))

def createStraightPillar(p1, p2):
    try:
        p1 = toNPArray3(p1)
        p2 = toNPArray3(p2)

        center = (p1 + p2) / 2
        height = np.linalg.norm(p1-p2)
        if height < epsilon:
            return DetailedCSG(None, ("Empty StraightPillar", p1, p2))

        radius = np.array([pillarCrossWidth * layerHeightMM, pillarCrossLength * layerHeightMM, height / 2])
        m1 = pymesh.generate_box_mesh(-radius, +radius)
        radius = np.array([pillarCrossLength * layerHeightMM, pillarCrossWidth * layerHeightMM, height / 2])
        m2 = pymesh.generate_box_mesh(-radius, +radius)
        result = pymesh.boolean(m1, m2, "union")
        result = rotate(result, [0, 0, 1], 45)
        result = translate(result, center[0], center[1], center[2])

        return DetailedCSG(result, ("StraightPillar", p1, p2))
    except:
        return DetailedCSG(None, ("Failed StraightPillar", p1, p2))

def createBridge(p1, p2, z):
    try:
        p1 = toNPArray3(p1)
        p2 = toNPArray3(p2)

        center = (p1 + p2) / 2
        p1 = np.concatenate([p1[:2], [0]])
        p2 = np.concatenate([p2[:2], [0]])
        length = np.linalg.norm(p1-p2)
        if length < epsilon:
            return DetailedCSG(None, ("Empty Bridge", p1, p2))

        c1 = np.array([0, layerHeightMM, 0])
        radius = np.array([length / 2, layerRadius, layerRadius])
        m1 = pymesh.generate_box_mesh(c1-radius, c1+radius)
        c2 = np.array([0, -layerHeightMM, 0])
        radius = np.array([length / 2, layerRadius, layerRadius])
        m2 = pymesh.generate_box_mesh(c2-radius, c2+radius)
        result = pymesh.boolean(m1, m2, "union")
        
        directionVector = p2 - p1
        rot = pymesh.Quaternion.fromData([1, 0, 0], directionVector)
        rot = rot.to_matrix()
        result = pymesh.form_mesh(np.dot(rot, (result.vertices).T).T, result.faces)

        result = translate(result, center[0], center[1], z)

        return DetailedCSG(result, ("Bridge", p1, p2, z))
    except:
        return DetailedCSG(None, ("Failed Bridge", p1, p2, z))

def createBridge2(bridge):
    return createBridge(bridge.p1, bridge.p2, bridge.p1.z + layerHeightMM)

def produce3D(csgs):
    print("Producing 3D")

    meshes = [{"mesh": csg.csg} for csg in csgs if not csg.csg is None]
    print("Total mesges to union: {0}".format(len(meshes)))
    tree = pymesh.CSGTree({"union":meshes});
    print("Support is ready to save")
    return DetailedCSG(tree.mesh, "Full CSG")

def produce3DFromPickle(path, num_of_parts=None):
    import pickle
    with open(path, "rb") as csgs_file:
      csgs = pickle.load(csgs_file)

    csgBuildFunction = {
        "Tip": createTip,
        "StraightPillar": createStraightPillar,
        "Bridge": createBridge
    }

    print("Totals objects in {0}: {1}".format(path, len(csgs)))
   
    newCsgs = []
    for csgDetails in csgs:
        csgType = csgDetails[0]
        csgArgs = csgDetails[1:]
        csgArgs = [arg if type(arg) in [int, float] else np.array(arg) for arg in csgArgs]
        if csgType in csgBuildFunction.keys():
            newCsgs.append(csgBuildFunction[csgType](*csgArgs))
        else:
            print("Unknown csg type: {0}".format(csgType))

    if not num_of_parts is None:
        newCsgs = newCsgs[:num_of_parts]
    
    return produce3D(newCsgs).csg

def saveCSGPickle(csgs, pklPath):
    import pickle
    with open(pklPath, "wb") as csgs_file:

      def simplifyArg(arg):
        if hasattr(arg, "x"):
            arg = toPoint3(arg)
            return [arg.x, arg.y, arg.z]

        return arg
      
      pickle.dump([[simplifyArg(arg) for arg in c.details] for c in csgs], csgs_file)

def saveSTL(csg, path):
    pymesh.save_mesh(path, csg)