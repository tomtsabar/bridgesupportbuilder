from .utils import *
from .anchoringSegments import *
from .csgUtils import *
from euclid3 import Vector3, Point3, Line2, LineSegment2
import math
from tqdm import tqdm

import matplotlib.pyplot as plt

DEBUG = False

def draw(points, bridges, anchoringSegments, currentSegments, events, event, i, n):
  plt.clf()
  for s in anchoringSegments:
    plt.plot([s.p1.x, s.p2.x], [s.p1.y, s.p2.y], color="green")
  for s in currentSegments:
    plt.plot([s.p1.x, s.p2.x], [s.p1.y, s.p2.y], color="blue")
  for b in bridges:
    plt.plot([b.p1.x, b.p2.x], [b.p1.y, b.p2.y], color="black")
  plt.scatter([p.x for p in points if p.is_open], [p.y for p in points if p.is_open], s=35, color="red")
  plt.scatter([p.x for p in points if not p.is_open], [p.y for p in points if not p.is_open], s=35, color="cyan")

  ylim = list(plt.gca().get_ylim())
  xlim = list(plt.gca().get_xlim())

  w = xlim[1] - xlim[0]
  h = ylim[1] - ylim[0]
  if w > h:
    ylim[0] -= (w - h) / 2
    ylim[1] += (w - h) / 2
  else:
    xlim[0] -= (h - w) / 2
    xlim[1] += (h - w) / 2

  bounds = [
  LineSegment2(Point2(xlim[0], ylim[0]), Point2(xlim[0], ylim[1])),
  LineSegment2(Point2(xlim[1], ylim[0]), Point2(xlim[1], ylim[1])),
  LineSegment2(Point2(xlim[0], ylim[0]), Point2(xlim[1], ylim[0])),
  LineSegment2(Point2(xlim[0], ylim[1]), Point2(xlim[1], ylim[1])),
  ]
  intersections = [event.sweepLine.intersect(b) for b in bounds]
  intersections = list(filter(None, intersections))

  plt.plot([intersections[0].x, intersections[1].x], [intersections[0].y, intersections[1].y], color="purple")

  plt.ylim(*ylim)
  plt.xlim(*xlim)

  plt.show()

def test_bridge_builder():
  points = [Point3(0, 0, 150), Point3(15, 0, 150), Point3(12, 10, 150), Point3(15, -10, 150)]

  # points = [Point3(x * 3, y * 5, 80) for x in range(10) for y in range(3)]

  bridges = []
  csg = generateScaffolding(points, bridges, "bridgesupportbuilder/models/test.pkl")
  saveSTL(csg, "bridgesupportbuilder/models/test.stl")

class Event:
  def __init__(self, sweepLine, sweepDirection, order, startingSegments, endingSegments):
    self.sweepDirection = sweepDirection
    self.sweepLine = sweepLine
    self.order = order
    self.startingSegments = startingSegments
    self.endingSegments = endingSegments

  @classmethod
  def createFromPoint(self, p, directionVector, startingSegments, endingSegments):
    sweepDirection, sweepLine = getSweepLine(directionVector, p + toVector2(directionVector * epsilon))
    order = directionVector.dot(toVector3(p))

    return Event(sweepLine, sweepDirection, order, startingSegments, endingSegments)

def fromXYZFile(path, pklPath=None):

  supportPoints = []
  with open(path, 'r') as fh:
    for lineText in fh.readlines():
      x, y, z, _ = lineText.split(' ')
      supportPoints.append (Point3(float(x), float(y), float(z)))

  return fromXYZ(supportPoints, [], pklPath)

def fromXYZ(points, bridges, pklPath=None):
  return generateScaffolding(points, bridges, pklPath)

def getSweepDirection(i):
  axes = Vector3(0, 0, 1)
  directionVector = Vector3(1, 0, 0)
  # Not 2 * pi becuase reverse directions will result with the same bridges
  return directionVector.rotate_around(axes, (i / sweepDirectionsAmount) * math.pi)

def getSweepLine(sweepDirectionVector, anchorPoint):
  axes = Vector3(0, 0, 1)
  sweepLineDirection = sweepDirectionVector.rotate_around(axes, math.pi / 2)
  return toVector2(sweepLineDirection), Line2(anchorPoint, toVector2(sweepLineDirection))

def generateScaffolding(supportPoints, bridges, pklPath=None):
  """
  supportPoints: a list of Point3D
  bridges: a list of Bridge
  """
  print("Searching for bridges")

  csgs = []
  supportPoints = [SupportedPoint(p.x, p.y, p.z) for p in supportPoints]
  totalOpenPoints = len(supportPoints) + len(bridges) * 2
  print("Total open points: {0}".format(totalOpenPoints))
  while True:
    bestBridge = None
    bestScore = None
    for i in range(sweepDirectionsAmount):
      directionVector = getSweepDirection(i)
      anchoringSegments = createAnchoringSegments(supportPoints, bridges, directionVector)
      currentSegments = []
      events = getEvents(anchoringSegments, directionVector, supportPoints + [b.p1 for b in bridges] + [b.p2 for b in bridges])

      print("Events: {0}".format(len(events)))
      with tqdm(total=len(events)) as pbar:
        while len(events) != 0:
          leftmostEvent = events.pop()
          currentSegments.extend(leftmostEvent.startingSegments)

          selectedBridge, selectedBridgeScore = selectBridge(currentSegments, directionVector, leftmostEvent)

          if DEBUG:
            if pbar.n % 80 in [0, 5]:
              draw(supportPoints, bridges, anchoringSegments, currentSegments, events, leftmostEvent, i, pbar.n)

          if bestBridge is None or (not selectedBridge is None and selectedBridgeScore > bestScore):
            bestBridge = selectedBridge
            bestScore = selectedBridgeScore

          for segment in leftmostEvent.endingSegments:
            if segment in currentSegments:
              currentSegments.remove(segment)

          pbar.update(1)

    if bestBridge is None:
      points = supportPoints + [b.p1 for b in bridges] + [b.p2 for b in bridges]
      for p in points:
        if p.is_open:
          csgs.append(createStraightPillar(p, Point3(p.x, p.y, 0)))

      for bridge in bridges:
        if not bridge.isBuilt:
          csgs.append(createBridge2(bridge))

      print("No more bridges with positive gain\n")

      if not pklPath is None:
        saveCSGPickle(csgs, pklPath)

      return produce3D(csgs).csg

    csgs.extend(snap(bestBridge))
    totalOpenPoints += 2
    totalOpenPoints -= len(bestBridge.intersections)
    print("Total open points: {0}".format(totalOpenPoints))

    newBridgeP1 = toSupportedPoint(bestBridge.intersections[0].p)
    newBridgeP2 = toSupportedPoint(bestBridge.intersections[-1].p)
    newBridgeP1.z = bestBridge.z
    newBridgeP2.z = bestBridge.z
    bridges.append(Bridge(newBridgeP1, newBridgeP2))
      
    if not pklPath is None:
      saveCSGPickle(csgs, pklPath)

    if DEBUG:
      csgs = [produce3D(csgs)]
      saveSTL(csgs[0].csg, "BridgeSupportBuilder/models/test_{0}.stl".format(totalOpenPoints))

def createAnchoringSegments(points, bridges, directionVector):
  points = list(points) # Clone
  bridges = list(bridges) # Clone
  anchoringSegments = []
  for bridge in bridges:
    if bridge.length >= longestBridge:
      continue

    extraLength = longestBridge - bridge.length

    if bridge.p1.is_open:
      points.append(bridge.p1)
      anchoringSegments.append(SegmentForBridge(bridge, True, bridge.p1, (bridge.p1 - bridge.p2).normalize() * extraLength))
    if bridge.p2.is_open:
      points.append(bridge.p2)
      anchoringSegments.append(SegmentForBridge(bridge, False, bridge.p2, (bridge.p2 - bridge.p1).normalize() * extraLength))

  for point in points:
    if point.is_open:
      anchoringSegments.append(SegmentForPoint(point, point - directionVector * longestPointTip, point + directionVector * longestPointTip))
  
  return anchoringSegments

def getEvents(anchoringSegments, directionVector, points):
  events = []
  for segment in anchoringSegments:
    p1_order = directionVector.dot(toVector3(segment.p1))
    p2_order = directionVector.dot(toVector3(segment.p2))
    if p1_order <= p2_order:
      events.append(Event.createFromPoint(segment.p1, directionVector, startingSegments=[segment], endingSegments=[]))
      events.append(Event.createFromPoint(segment.p2, directionVector, startingSegments=[], endingSegments=[segment]))
    else:
      events.append(Event.createFromPoint(segment.p2, directionVector, startingSegments=[segment], endingSegments=[]))
      events.append(Event.createFromPoint(segment.p1, directionVector, startingSegments=[], endingSegments=[segment]))

  for p in points:
    if p.is_open:
      events.append(Event.createFromPoint(toPoint2(p), directionVector, startingSegments=[], endingSegments=[])) # Not originaly in the paper

  intersections = getIntersections(anchoringSegments)
  events.extend([Event.createFromPoint(intersection, directionVector, [], []) for intersection in intersections])
  events.sort(key=lambda event: event.order) # This is the reversed order, but the next loop reverses the list

  if len(events) == 0:
    return []

  result = []
  current_event = events.pop()
  while len(events) != 0:
    next_event = events.pop()
    if current_event.order == next_event.order:
      current_event.startingSegments.extend(next_event.startingSegments)
      current_event.endingSegments.extend(next_event.endingSegments)
    else:
      result.append(current_event)
      current_event = next_event

  return result

def getIntersections(segments):
  intersections = []
  for i in range(len(segments)):
    s1 = segments[i]
    for j in range(i + 1, len(segments)):
      s2 = segments[j]
      intersection = s1.intersect(s2) # Intersect is not implemented for euclid.LineSegment3
      if intersection:
        intersections.append(intersection)

  return intersections

def selectBridge(segments, directionVector, event):
  intersections = [segment.intersectSweepLine(event.sweepLine, directionVector) for segment in segments]
  intersections = [i for i in intersections if not i is None]
  intersections.sort(key=lambda intersection: intersection.order)
  possibleBridgeHeights = [intersection.maxZ for intersection in intersections if intersection.maxZ > 0]

  if not optionalBridgeHeightResolution is None:
    r = optionalBridgeHeightResolution
    possibleBridgeHeights = [r * int(h / r) for h in possibleBridgeHeights]

  possibleBridgeHeights = list(set(possibleBridgeHeights))

  bestBridge = None
  bestScore = None
  for z in possibleBridgeHeights:
    for i in range(len(intersections)):
      supportedPoints = []
      for j in range(i, len(intersections)):
        if intersections[i].p.distance(intersections[j].p) <= longestBridge:
          supportedPoints.append(intersections[j])
          if collisions(intersections, i, j, z):
            break
          gain, score = evalBridge(intersections, i, j, z)
          if gain > 0:
            if bestBridge is None or score > bestScore:
              bestBridge = getPossibleBridge(intersections, i, j, z)
              bestScore = score

  return bestBridge, bestScore

def evalBridge(intersections, i, j, z):
  lMax = 0
  k = 0
  points = []
  for intersection in intersections[i : j + 1]:
    if z <= intersection.maxZ:
      points.append(intersection.segment.point)
      l = (intersection.maxZ - z) + intersection.tipLength
      lMax = max(lMax, l)

  k = len(set([(p.x, p.y) for p in points]))

  bridgeWidth = toPoint2(intersections[i].p).distance(toPoint2(intersections[j].p))

  gain = (k - 2) * z - bridgeWidth
  score = gain - k * lMax
  
  return gain, score

def getPossibleBridge(intersections, i, j, z):
  class PossibleBridge:
    def __init__(self, z):
      self.z = z
      self.intersections = []

  bridge = PossibleBridge(z)
  for intersection in intersections[i : j + 1]:
    if z <= intersection.maxZ:
      bridge.intersections.append(intersection)
  
  return bridge

def collisions(intersections, i, j, z):
  return False

def snap(bridge):
  csgs = []
  for intersection in bridge.intersections:
    csgs.extend(intersection.segment.snap(intersection, bridge.z))

  csgs.append(createBridge(bridge.intersections[0].p, bridge.intersections[-1].p, bridge.z))

  return csgs
